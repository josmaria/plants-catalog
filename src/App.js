import './App.css';
import Product from './components/Product';

function App() {
  return (
    <div className='App'>
      <div className='container-principal'>
        <h1>Catalogo</h1>
        <div className='container-products'>
          <Product 
            image='one'
            commonName='Acacia orrida'
            scientistName='Acacia orrida'
            family='Fabaceae' />
          <Product 
            image='two'
            commonName='Aji ornamental'
            scientistName='Capsicum annuum; L.'
            family='Solanaceae' />
          <Product 
            image='three'
            commonName='Ajuga'
            scientistName='Ajuga reptans'
            family='Lamiaceae' />
          <Product 
            image='four'
            commonName='Nombre Comun'
            scientistName='Nombre Cientifico'
            family='Familia' />
          <Product 
            image='five'
            commonName='Nombre Comun'
            scientistName='Nombre Cientifico'
            family='Familia' />
          <Product 
            image='six'
            commonName='Nombre Comun'
            scientistName='Nombre Cientifico'
            family='Familia' />
          <Product 
            image='one'
            commonName='Nombre Comun'
            scientistName='Nombre Cientifico'
            family='Familia' />
          <Product 
            image='two'
            commonName='Nombre Comun'
            scientistName='Nombre Cientifico'
            family='Familia' />
          <Product 
            image='three'
            commonName='Nombre Comun'
            scientistName='Nombre Cientifico'
            family='Familia' />
        </div> 
      </div>
    </div>
  );
}

export default App;
