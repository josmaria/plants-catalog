import '../stylesheets/Testimony.css'

export function Testimony(props) {
  return (
    <div className='container-testimony'>
        <img 
          className='image-testimony'
          src={require(`../images/plant-number-${props.image}.png`)}
          alt='Plant Number One' />
        <div className='container-text-testimony'>
          <p className='name-testimony'>{props.name} en {props.country}</p>
          <p className='occupation-testimony'>{props.occupation} en {props.business}</p>
          <p className='text-testimony'>{props.testimony}</p>
        </div>
    </div>
  );
}
