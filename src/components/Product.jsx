import '../stylesheets/Product.css'

function Product(props) {
  return (
    <div className='container-product'>
      <img 
        className='image-product'
        src={require(`../images/plant-number-${props.image}.png`)}
        alt='Plant Number One'/>
      <div className='container-information-catalog'>
        <p className='name-common'>{props.commonName}</p>
        <p className='name-scientist'><i>{props.scientistName}</i></p>
        <p className='family'>{props.family}</p>
      </div>
    </div>
  );
}

export default Product;